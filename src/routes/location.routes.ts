import { Router } from 'express';
const router = Router();

import { getLocation } from './location.controller';

router.get('/location', getLocation);


export default router;