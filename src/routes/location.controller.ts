import { AnyARecord } from 'dns';
import {RequestHandler} from 'express';

var geoip = require('geoip-lite');

export const getLocation: RequestHandler = (req, res) => {
    var toSendBack: any;
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	//mx
	//var ip = "177.248.196.196";
    //usa
	//var ip = "207.97.227.239"; 
	var geo = geoip.lookup(ip);	

	if (geo)
		toSendBack = geo.country;
	else
		toSendBack = "Unknown";
	
	res.send(toSendBack);
	//res.json(geoip.pretty(ip));
}

