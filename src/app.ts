import cors from 'cors';
import express from 'express';
import morgan from 'morgan';
import config from './config';

import location from './routes/location.routes';

const app = express();

app.set('port', config.PORT);

app.use(morgan('dev'));

app.use(cors());

app.use(express.json()); 

app.use(location);

app.use(express.json());


export default app;